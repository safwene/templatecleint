import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from './home/home.component';
import {LayoutComponent} from './home/layout/layout.component';
import {ErreurComponent} from './erreur/erreur.component';
import {LoginComponent} from './login/login.component';
import {ProduitComponent} from './produit/produit.component';
import {ProduitDetailComponent} from './produit-detail/produit-detail.component';
import {CheckoutComponent} from './checkout/checkout.component';
import {CartComponent} from './cart/cart.component';
import {ContactComponent} from './contact/contact.component';

const routes: Routes = [
  {path: '', component: HomeComponent,
  children: [

  {path: '', component: LayoutComponent},
  {path: 'login', component: LoginComponent},
  {path: 'produit', component: ProduitComponent},
  {path: 'produitDetail', component: ProduitDetailComponent},
  {path: 'contact', component: ContactComponent},
  {path: 'checkout', component: CheckoutComponent},
  {path: 'cart', component: CartComponent},
  {path: '**', component: ErreurComponent}
  ],
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
