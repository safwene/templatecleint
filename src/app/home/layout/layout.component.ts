import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {ProduitService} from '../../service/produit.service';
import {ImageServiceService} from '../../service/image-service.service';
import {AuthentificationService} from '../../service/authentification.service';
import {CategorieService} from '../../service/categorie.service';
import {SousCategorieService} from '../../service/sous-categorie.service';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent implements OnInit {
  resulta;
  filesToUpload: Array<File>;
  photo;
  categories;
  souscategories;




  constructor(private produitService: ProduitService, private  router: Router, private imageservice: ImageServiceService, private authService: AuthentificationService, private categoService: CategorieService, private souscategoService: SousCategorieService) {

  const data = {

    username: 'client1' ,
     password: '1234567'

   };
  this.login(data);
  this.all();
  this.allcategorie();

  }


  ngOnInit() {
  }
login(data) {
  return this.authService.login(data).subscribe(res => {
    const jwt = res.headers.get('Authorization');
    this.authService.saveToken(jwt);

    console.log(res);

 });
}

  all() {

    this.produitService.getAllProduit().subscribe(res => {
      // this.authService.loadToken();
      console.log(res);
      this.resulta = res;
      // this.catego = res.get;

    });
  }
  recuperFile(file) {
    this.filesToUpload = file.target.files as Array<File>;

    this.photo = file.target.files[0].name;
  }
  allcategorie() {
return this.categoService.getAllCategories().subscribe(r => {

  console.log(r);
  this.categories = r;
});
  }
  allSousCategories(id) {
    return this.souscategoService.getAllSousCategories(id).subscribe(t => {
      console.log(t);
      this.souscategories = t;
    });
  }
}
