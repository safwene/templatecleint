import { Component, OnInit } from '@angular/core';
import {ProduitService} from '../service/produit.service';

@Component({
  selector: 'app-produit',
  templateUrl: './produit.component.html',
  styleUrls: ['./produit.component.css']
})
export class ProduitComponent implements OnInit {
  resulta;
  filesToUpload: Array<File>;
  photo;
  constructor(private produitService: ProduitService) {
    this.all();
  }

  ngOnInit() {
  }
  all() {

    this.produitService.getAllProduit().subscribe(res => {
      // this.authService.loadToken();
      console.log(res);
      this.resulta = res;
      // this.catego = res.get;

    });
  }
  recuperFile(file) {
    this.filesToUpload = file.target.files as Array<File>;

    this.photo = file.target.files[0].name;
  }
}
