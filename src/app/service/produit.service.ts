import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Router} from '@angular/router';
import {AuthentificationService} from './authentification.service';

@Injectable({
  providedIn: 'root'
})
export class ProduitService {

  constructor(private http: HttpClient, private router: Router, private authservice: AuthentificationService) {
  }

  getAllProduit() {
    const header = new HttpHeaders({Authorization: 'Bearer' + this.authservice.jwt});
    return this.http.get('http://localhost:9000/produit/all',{headers : header});
  }

}
