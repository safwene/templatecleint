import { Injectable } from '@angular/core';
import {AuthentificationService} from './authentification.service';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SousCategorieService {

  constructor(private http: HttpClient, private authservice: AuthentificationService) { }
  getAllSousCategories(id) {
    const header = new HttpHeaders({Authorization: 'Bearer ' + this.authservice.jwt});
    return this.http.get('http://localhost:9000/sousCategorie/getall/' + id, {headers: header});
  }
}
