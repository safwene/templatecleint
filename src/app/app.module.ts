import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './home/header/header.component';
import { LayoutComponent } from './home/layout/layout.component';
import { FooterComponent } from './home/footer/footer.component';
import { ErreurComponent } from './erreur/erreur.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ProduitComponent } from './produit/produit.component';
import { ProduitDetailComponent } from './produit-detail/produit-detail.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { CartComponent } from './cart/cart.component';
import { ContactComponent } from './contact/contact.component';
import { BloglistComponent } from './bloglist/bloglist.component';
import { BlogsingleComponent } from './blogsingle/blogsingle.component';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    LayoutComponent,
    FooterComponent,
    ErreurComponent,
    LoginComponent,
    RegisterComponent,
    ProduitComponent,
    ProduitDetailComponent,
    CheckoutComponent,
    CartComponent,
    ContactComponent,
    BloglistComponent,
    BlogsingleComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
